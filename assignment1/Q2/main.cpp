// Implememtation of std::deque
#include <iostream>

using namespace std;

int INDEX_BUFFER_SIZE = 8;
int NUMBER_OF_ELEMENT_IN_A_CHUNK=16;


template <typename T> 
class Deque {
    private: 
    int _NUMBER_OF_ELEMENT_IN_A_CHUNK = NUMBER_OF_ELEMENT_IN_A_CHUNK;
    int _sizeOfT;
    int _front=0;
    int _back=-1;
    int _noOfChunks=0;
    int _currentMaxBufferSize=INDEX_BUFFER_SIZE;
    // vector<T *> chunks;
    T ** chunks;

    void printMessage(string msg) {
        cout << "\n### " << msg << " ###\n";
    }

    void doubleAndCopyChunksArray(){
        _currentMaxBufferSize *= 2;
        T ** temp = new T*[_currentMaxBufferSize];
        for(int i=0;i<_noOfChunks;i++){
            temp[i] = chunks[i];
        }
        T ** deleteChunks = chunks;
        chunks = temp;
        cout << "Address space doubled to\n" << _currentMaxBufferSize << endl;;
        delete deleteChunks;
    }

    void createChunkBack(int numberOfChunk){
        if(numberOfChunk == 0) return;
        int n = numberOfChunk;
        while(n--){
            T* chunk = new T[_NUMBER_OF_ELEMENT_IN_A_CHUNK];
            //_chunks.push_back(chunk);
            chunks[_noOfChunks] = chunk;
            _noOfChunks++;
            if(_noOfChunks == _currentMaxBufferSize-1){
                // increase array size
                doubleAndCopyChunksArray();
            }
        }
        // printMessage(to_string(numberOfChunk) + " new chunk created at back.");
    }

    // always only one chunk will be added at front
    void createChunkFront(){
        T* chunk = new T[_NUMBER_OF_ELEMENT_IN_A_CHUNK];
        // chunks.insert(chunks.begin(), chunk);
        for(int i=_noOfChunks-1;i>=0;i--){
             chunks[i+1] = chunks[i];
        }
        chunks[0] = chunk;
        _noOfChunks++;
        if(_noOfChunks == _currentMaxBufferSize -1){
            // increase array size
            doubleAndCopyChunksArray();
        }
        // printMessage("1 new chunk created at front.");
    }
    
    void construct(){
        _sizeOfT = sizeof(T);
        // chunks.clear();
        if(_noOfChunks){
            delete chunks;
        }
        _front=0;
        _back=-1;
        _noOfChunks=0;
        _NUMBER_OF_ELEMENT_IN_A_CHUNK = NUMBER_OF_ELEMENT_IN_A_CHUNK;
        chunks = new T*[INDEX_BUFFER_SIZE];
        _currentMaxBufferSize=INDEX_BUFFER_SIZE;
        createChunkBack(1);
    }


    
    void fillChunks(T initialValue, int extraElementReqdAtLast){
        for(int i=0;i<_noOfChunks - 1;i++){
            for(int j=0;j<_NUMBER_OF_ELEMENT_IN_A_CHUNK;j++){
                chunks[i][j] = initialValue;
            }
        }
        // For last chunk;
        for(int j=0;j<extraElementReqdAtLast;j++){
            _back++;
            chunks[_noOfChunks - 1][j] = initialValue;
        }
    }
    
    void exitProgram(string message, int code){
        printMessage(message);
        exit(code);
    }

    
    void printCurrentPrivateVars(){
        cout << "\n#_noOfChunks: " << _noOfChunks;
        cout << "\n#_front: " << _front;
        cout << "\n#_back: " << _back;
        cout << "\n#_size:" << size() << "\n";
        for(int i=0;i<_noOfChunks;i++){
            cout << chunks[i] << " ";
        }
        cout << endl;
    }
    
    void printDeque(){
        if(_noOfChunks == 0) return;
        if(_noOfChunks == 1) {
            for(int j=_front;j<=_back;j++){
                cout << chunks[0][j] << " ";
            }
            return;
        }
        for(int i=0;i<_noOfChunks;i++){
            if(i==0){
                for(int j=_front;j<_NUMBER_OF_ELEMENT_IN_A_CHUNK;j++){
                    cout << chunks[i][j] << " ";
                }  
            }
            if(i!=0 && i!=_noOfChunks-1){
                for(int j=0;j<_NUMBER_OF_ELEMENT_IN_A_CHUNK;j++){
                    cout << chunks[i][j] << " ";
                }
            }
            if(i==_noOfChunks-1){
                for(int j=0;j<=_back;j++){
                    cout << chunks[i][j] << " ";
                } 
            }
            cout << "\n";
        }
    }

    public : 
        Deque(){
           construct(); 
        }

        Deque(int size, T initialValue){
            construct(); 
            int noOfChunkRequired = size / _NUMBER_OF_ELEMENT_IN_A_CHUNK + 1;
            int extraElementReqdAtLast = size % _NUMBER_OF_ELEMENT_IN_A_CHUNK;
            createChunkBack(noOfChunkRequired - 1); // first chunk is automatically created in construct
            fillChunks(initialValue,extraElementReqdAtLast);
        }

        bool empty(){
            if(_noOfChunks == 1 && _front>_back){
                return true;
            }return false;
        }

        int size(){
            if(empty()) return 0;
            if(_noOfChunks==1) return _back - _front + 1;
            int firstChunkEls = _NUMBER_OF_ELEMENT_IN_A_CHUNK - _front;
            int lastChunkEls =  _back+1;
            int remElms = (_noOfChunks-2) * _NUMBER_OF_ELEMENT_IN_A_CHUNK;
            return (firstChunkEls + lastChunkEls + remElms);
            
        }

        // Operator overloading for []
        T operator[](int specifierIndex){
            int index = specifierIndex + _front;
            int chunkNo = index / _NUMBER_OF_ELEMENT_IN_A_CHUNK;
            if(chunkNo >= _noOfChunks || chunkNo <= -1){
                // exitProgram("Invalid access to index " + to_string(index),0);
                cout << "Out of bound access\n";
                T v = T() ;
                return v;
            }
            int elementNo = (index) % _NUMBER_OF_ELEMENT_IN_A_CHUNK;
            if(chunkNo == _noOfChunks-1 && elementNo > _back){
                // exitProgram("Invalid access to index " + to_string(index),0);
                cout << "Out of bound access\n";
                 T v = T() ;
                return v;
            }
            return chunks[chunkNo][elementNo];
        }

        void push_back(T value){
            // if new chunk is required at front
            if(_back == _NUMBER_OF_ELEMENT_IN_A_CHUNK-1){
                createChunkBack(1);
                _back=-1;
            }
            cout << value;
            _back++;
            chunks[_noOfChunks-1][_back] = value;  
        }

        void pop_back(){
            if(empty()){
                printMessage("Deque is empty.");
                return ;
            }
            // if _back is on top of the chunk
            if(_noOfChunks > 1 && _back==0){
                // delete [] chunks[_noOfChunks-1];
                cout << "Freed array " <<  chunks[_noOfChunks-1] << " from back\n";
                // chunks.pop_back();
                _noOfChunks--;
                _back  = _NUMBER_OF_ELEMENT_IN_A_CHUNK - 1;
                return ;
                
            }
            _back--;
        }

        T back(){
            if(empty()){
                cout << "Deque is empty\n";
                return T() ;
            }
            return chunks[_noOfChunks-1][_back];
        }

        void push_front(T value){
            // handle special case for push_front as first operation in empty deque
            // here we will not create new array just modify _front and _back
            if(empty()){
                chunks[0][_front] = value; 
                _back=0;
                return ;
            }
            // if front=0, new chunk need to be allocated and linked in front
            if(_front == 0 ){
                createChunkFront();
                _front = _NUMBER_OF_ELEMENT_IN_A_CHUNK - 1;
                chunks[0][_front] = value; 
                return ;
            }
            _front--;
            chunks[0][_front] = value; 
            return ; 
        }

        void pop_front(){
            if(empty()){
                printMessage("Deque is empty.");
                return ;
            }
            // if _front is on end of the chunk
            if(_front == _NUMBER_OF_ELEMENT_IN_A_CHUNK - 1){
                // delete [] chunks[0];
                cout << "Freed array " <<  chunks[0] << " from front\n";
                // chunks.erase(chunks.begin());
                 for(int i=0;i<_noOfChunks-1;i++){
                    chunks[i]=chunks[i+1];
                }
                _noOfChunks--;
                _front  = 0;
                return ;
            }
            _front++;
        }

        T front(){
            if(empty()){
               cout << "Deque is empty\n";
               return T() ;
            }
            return chunks[0][_front];
        }

        void resize(int newSize , T defaultValue){
            if(newSize > size()){
                while(size() != newSize){
                    push_back(defaultValue);
                }
            }else{
                while(size() != newSize){
                    pop_back();
                }
            }
        }

        void clear(){
            for(int i=0;i<_noOfChunks;i++){
                delete [] chunks[i];
                cout << "\nFreed array " <<  chunks[i] << "\n";
            }
            construct();
        }

        void printDetails(){
            cout << "\n---------------------------------------------------------\n";
            printDeque();
            printCurrentPrivateVars();
            cout << "\n---------------------------------------------------------\n";
        }
};
void printMenu();

template <typename T>
void serve(int input,Deque<T>& dq){
    cout << endl;
    switch(input){
        case 0: 
            exit(0);   
        case 1:
            return;
        case 2:
            return;
        case 3:{
            cout << "3. push_back(x)\n";
            cout << "Enter the value\n";
            T value;
            cin >> value;
            dq.push_back(value);
            break;
        }
        case 4:{
            cout << "4. pop_back()\t";
            dq.pop_back();
            break;
        }
        case 5:{
            cout << "5. push_front(x)\n";
            cout << "Enter the value\n";
            T value;
            cin >> value;
            dq.push_front(value);
            break;
        }
        case 6:{
            cout << "6. pop_front()\n";
            dq.pop_front();
            break;
        }
        case 7:{
            cout << "7. front()\t";
            cout << dq.front();
            break;
        }
        case 8:{
            cout << "8. back()\t";
            cout << dq.back();
            break;
        }
        case 9:{
            cout << "9. D[n]\t";
            cout << "Enter the index to be accessed\n";
            int value;
            cin >> value;
            cout << "\n";
            cout << dq[value];
            break;
        }
        case 10:{
            cout << "10. empty()\t";
            cout << dq.empty() ? true : false;
            break;
        }
        case 11:{
            cout << "11. size()\t";
            cout << dq.size();
            break;
        }
        case 12:{
            cout << "12. resize(x, d)\n";
            int x;T d;
            cout << "Enter the new size and default value\n";
            cin >> x >> d;
            dq.resize(x,d);
            break;
        }
        case 13:{
            cout << "13. clear()\n";
            dq.clear();
            break;
        } 
        case 14:{
            cout << "14. Print details\n";
            dq.printDetails();
            break;
        } 
        default:
            break;;
    }
    cout << endl;
}

int main(){
    Deque<float> dq;
    // Deque<int> dq(12,0);
    int input=-1;
    while(input!=0){
        printMenu();
        cin >> input;
        serve<float>(input,dq); // change this to be dynamic type detection
        dq.printDetails();
    }
    // for(int i=0;i<1000;i++){
    //     dq.push_back(i);
    // }
    // for(int i=0;i<283;i++){
    //     dq.pop_front();
    // }
    // for(int i=0;i<2500;i++){
    //     dq.push_front(-i);
    // }
    // for(int i=0;i<203;i++){
    //     dq.pop_front();
    // }
    // cout << dq[3];
    // dq.printDetails();
}

void printMenu(){
    cout << "\n-------------------------------------------------------\n";
    cout << "Select one of the options\n";
    cout << "0. Quit\n";
    cout << "1. deque\n";
    cout << "2. deque(n,x)\n";
    cout << "3. push_back(x)\n";
    cout << "4. pop_back()\n";
    cout << "5. push_front(x)\n";
    cout << "6. pop_front()\n";
    cout << "7. front()\n";
    cout << "8. back()\n";
    cout << "9. D[n]\n";
    cout << "10. empty()\n";
    cout << "11. size()\n";
    cout << "12. resize(x, d)\n";
    cout << "13. clear()\n";
    cout << "14. Print details()\n";
    cout << "\n-------------------------------------------------------\n";
}
