#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int MAX = 10000;

void print3d(int ***rgb, int H, int W, int C){
    int i, j,k;
    for (i = 0; i < H; i++){
        for (j = 0; j < W; j++){
            for(k=0; k<C; k++){
                printf("%d ",rgb[i][j][k]);
            }
            cout << "  ";      
        }
    cout << "\n";
    }
}

void print(int *arr, int m, int n)
{
    int i, j;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
             printf("%d ", *((arr+i*n) + j));
        }
    cout << "\n";
    }
    cout << endl << endl ;
}

int square(int a){
    return a * a;
}

double minm(double x,double y,double z){
    double smallest = -1;

    if (x < smallest)
    smallest=x;
    if (y < smallest)
    smallest=y;
    if(z < smallest)
    smallest=z;

    return smallest;
}

void createGradientImage(double **gradient, int H, int W, int C){
    string op_dir = "../data/output/";
    string op_file = "gradient_out.txt";
    ofstream fout(op_dir + op_file);
    
    fout << H << " " << W << " " << C << endl;
    for(int i = 0; i < H; ++i) {
        for(int j = 0; j < W; ++j) {
                fout << (int)gradient[i][j] << " " << 0 << " " << 0 << " " ;

        }
        fout << '\n';
    }
    fout.close();
}

void fillGradientMatrix(int ***rgb,double **gradient,int H, int W, int C){
    int l,r,u,d;
    int xDelSq,yDelSq;
    for(int i = 0; i < H; ++i) {
        for(int j = 0; j < W; ++j) {
            r = j+1;
            l = j-1;
            d = i+1;
            u = i-1;
            if(i==0){
                u = H-1;
            }
            if(i==H-1){
                d = 0;
            }
            if(j==0) {
                l = W-1;
            }
            if(j==W-1){
                r = 0;
            }
            // Calculating dual gradient energy function
            xDelSq = square(rgb[i][r][0] - rgb[i][l][0])
                + square(rgb[i][r][1] - rgb[i][l][1])
                + square(rgb[i][r][2] - rgb[i][l][2]);

            yDelSq = square(rgb[d][j][0] - rgb[u][j][0])
                + square(rgb[d][j][1] - rgb[u][j][1])
                + square(rgb[d][j][2] - rgb[u][j][2]);

            gradient[i][j]  = sqrt(xDelSq + yDelSq);
        }
    }
}

void getMinSeamPath(double **gradient,int* backtrack,int H, int W, int C){
    double **dp;
    dp = new double *[H];
    for(int i = 0; i < H; ++i) {
        dp[i] = new double [W];
    }

    for(int m=0;m<W;m++){
        dp[0][m]=gradient[0][m];
    }
    double lu,uu,ru,min;
    for(int i=1;i<H;i++){
        for(int j=0;j<W;j++){
            if(j==0) {
                lu = MAX;
            }else{
                lu = dp[i-1][j-1];
            }
            if(j==W-1) {
                ru = MAX;
            }else{
                ru = dp[i-1][j+1];
            }
            uu = dp[i-1][j];
            min = minm(lu,uu,ru);
            dp[i][j] = gradient[i][j] + min;
        }
    }

    double minSeam = MAX;int minSeamIndex;
    for(int m=0;m<W;m++){
        if(dp[H-1][m] < minSeam){
            minSeam = dp[H-1][m];
            minSeamIndex = m;
        }
    }
    backtrack[H-1]=minSeamIndex;
    int lastMinIdx;
    int minCurIdx;
    double minCurValue =MAX;
    for(int m= H-2;m>=0;m--){
        lastMinIdx = backtrack[m+1];
        if(lastMinIdx==0) {
            lu = MAX;
        }else{
            lu = dp[m][lastMinIdx-1];
        }
        if(lu <  minCurValue){
           minCurValue = lu;
           minCurIdx = lastMinIdx -1; 
        }
        if(lastMinIdx==W-1) {
            ru = -1;
        }else{
            ru = dp[m][lastMinIdx+1];
        }
        if(ru <  minCurValue){
            minCurValue = ru;
            minCurIdx = lastMinIdx + 1; 
        }
        uu = dp[m][lastMinIdx];
        if(uu <  minCurValue){
            minCurValue = uu;
            minCurIdx = lastMinIdx; 
        }
        backtrack[m] = minCurIdx;
    }

    // free dp array
    for(int i = 0; i < H; ++i) {
        delete [] dp[i];
    }
        delete [] dp;
}

void saveCurrentImage(int ***rgb,int H,int W,int curWid,int C){
    string op_dir = "../data/output/in_between/";
    string op_file = "rgb_in_between";
    string format = ".txt";
    string index = to_string(W-curWid);
    ofstream fout(op_dir + op_file + index + format);
    
    fout << H << " " << curWid << " " << C << endl;
    for(int i = 0; i < H; ++i) {
        for(int j = 0; j < curWid; ++j) {
            for(int k = 0; k < C; ++k) {
                fout << rgb[i][j][k] << " ";
            }
        }
        fout << '\n';
    }
    fout.close();
}


void onePxSmallerRGBMatrix(int ***rgb,int *minSeamPath,int H, int W,int curWd, int C){
    for(int i=0;i<H;i++){
        int j = minSeamPath[i];
        rgb[i][j][0] = 255;
        rgb[i][j][1] = 0;
        rgb[i][j][2] = 0;
    }
    // saveCurrentImage(rgb,H,W,curWd,C);
     for(int i=0;i<H;i++){
        for(int j = minSeamPath[i] + 1;j<curWd;j++){
            rgb[i][j-1][0] = rgb[i][j][0];
            rgb[i][j-1][1] = rgb[i][j][1];
            rgb[i][j-1][2] = rgb[i][j][2];
        }
    }

}

void rotate90clockwise(int ***rgb,int ***rotRGB,int H, int W, int C){
    for (int i=0; i<H; i++){
        for (int j=0;j<W; j++){
            rotRGB[j][H-1-i][0] = rgb[i][j][0];
            rotRGB[j][H-1-i][1] = rgb[i][j][1];
            rotRGB[j][H-1-i][2] = rgb[i][j][2];
        }
    }
	
}

void rotate90AntiClockwise(int ***rgb,int ***rotRGB,int H, int W, int C){
    for (int i=0; i<W; i++){
        for (int j=0;j<H; j++){
            rgb[i][j][0] = rotRGB[j][W-1-i][0];
            rgb[i][j][1] = rotRGB[j][W-1-i][1];
            rgb[i][j][2] = rotRGB[j][W-1-i][2];
        }
    }
	
}
void seamHorizontal(int ***rgb, int H, int W, int C, int H_, int W_, int C_){
    double **gradient;
    gradient = new double *[H];
    for(int i = 0; i < H; ++i) {
        gradient[i] = new double [W];
    }

    int curWd = W;
    while(curWd >= W_){
        // Fill the gradient matrix
        fillGradientMatrix(rgb,gradient,H,curWd,C);

        // // Use gradient matrix to create gradient image
        createGradientImage(gradient,H,curWd,C);

        // Get the minimum seam path
        int *minSeamPath;
        minSeamPath = (int *)malloc(sizeof(int) * H);
        if(minSeamPath == NULL){
            cout << "No available space on heap\n";
            return ;
        }
        getMinSeamPath(gradient,minSeamPath,H,curWd,C);
        onePxSmallerRGBMatrix(rgb,minSeamPath,H,W,curWd,C);
        free(minSeamPath);
        curWd--;
    }
    // free gradient array
    for(int i = 0; i < H; ++i) {
        delete [] gradient[i];
    }
        delete [] gradient;
}

void saveRotatedImage(int ***rotRGB,int H, int W, int C){
    string op_dir = "../data/output/";
    string op_file = "rot_out.txt";
    ofstream fout(op_dir + op_file);

    fout << H << " " << W << " " << C << endl;
    for(int i = 0; i < H; ++i) {
        for(int j = 0; j < W; ++j) {
            for(int k = 0; k < C; ++k) {
                fout << rotRGB[i][j][k] << " ";
            }
        }
    fout << '\n';
    }
    fout.close();
}

int ***allocateRotRGB(int H,int W,int C,int W_){
    int ***rotRGB;
    rotRGB = new int **[W_];
    for(int i = 0; i < W_; ++i) {
        rotRGB[i] = new int *[H];
        for(int j=0;j<H;j++){
           rotRGB[i][j] = new int [C];
        }
    }
    return rotRGB;
}

void solve(int ***rgb,int H, int W, int C, int H_, int W_, int C_) {
    seamHorizontal(rgb,H,W,C,H_,W_,C_);
    int ***rotRGB = allocateRotRGB(H,W,C,W_);
    rotate90clockwise(rgb,rotRGB,H,W_,C);
    saveRotatedImage(rotRGB,W_,H,C);
    seamHorizontal(rotRGB,W_,H,C,W_,H_,C_);
    rotate90AntiClockwise(rgb,rotRGB,W_,H_,C_);

}

int main() {
    string ip_dir = "../data/input/";
    string ip_file = "rgb_in.txt";
    ifstream fin(ip_dir + ip_file);
    int H, W, C;
    fin >> H >> W >> C;
    int ***rgb;
    rgb = new int **[H];
    for(int i = 0; i < H; ++i) {
        rgb[i] = new int *[W];
        for(int j = 0; j < W; ++j) {
            rgb[i][j] = new int[C];
            for(int k = 0; k < C; ++k)
                fin >> rgb[i][j][k];
        }
    }
    fin.close();

    int H_, W_, C_;
    cout << "Enter new value for H (must be less than " << H << "): ";
    cin >> H_;
    cout << "Enter new value for W (must be less than " << W << "): ";
    cin >> W_;
    cout << '\n';
    C_ = C;

    solve(rgb, H, W, C, H_, W_, C_);

    string op_dir = "../data/output/";
    string op_file = "rgb_out.txt";
    ofstream fout(op_dir + op_file);
    
    fout << H_ << " " << W_ << " " << C_ << endl;
    for(int i = 0; i < H_; ++i) {
        for(int j = 0; j < W_; ++j) {
            for(int k = 0; k < C_; ++k) {
                fout << rgb[i][j][k] << " ";
            }
        }
        fout << '\n';
    }
    fout.close();

    return 0;
}